package axis.example.service;

import axis.example.domain.Contact;
import axis.example.domain.repository.ContactRepository;

public class ContactService 
{
	public Contact searchContactWithExactlyName(String name)
	{
		return new ContactRepository().findByName(name);
	}
}

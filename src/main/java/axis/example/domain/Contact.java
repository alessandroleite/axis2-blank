package axis.example.domain;

import java.util.Date;

public class Contact
{
    private String _name;
    private Date _birthdate;
    private String _email;

    /**
     * @return the name
     */
    public String getName()
    {
        return _name;
    }

    /**
     * @param name
     *            the name to set
     */
    public Contact setName(String name)
    {
        this._name = name;
        return this;
    }

    /**
     * @return the date
     */
    public Date getBirthdate()
    {
        return _birthdate;
    }

    /**
     * @param date
     *            the date to set
     */
    public Contact setBirthdate(Date date)
    {
        this._birthdate = date;
        return this;
    }

    /**
     * @return the email
     */
    public String getEmail()
    {
        return _email;
    }

    /**
     * @param email
     *            the email to set
     */
    public Contact setEmail(String email)
    {
        this._email = email;
        return this;
    }
}

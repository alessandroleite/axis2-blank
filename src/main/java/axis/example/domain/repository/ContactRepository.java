package axis.example.domain.repository;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import axis.example.domain.Contact;

public class ContactRepository 
{
	private static final Map CONTACTS = new HashMap();
	
	{
		CONTACTS.put("foo", new Contact().setName("foo").setBirthdate(new Date()).setEmail("foo@acme.org"));
		CONTACTS.put("bar", new Contact().setName("bar").setBirthdate(new Date()).setEmail("bar@acme.org"));
	}

	public Contact findByName(String name) 
	{
		return (Contact) CONTACTS.get(name != null ? name.trim().toLowerCase() : "");
	}

	public void insert(Contact contact) 
	{
		if (contact != null && contact.getName() != null)
		{
			CONTACTS.put(contact.getName().trim().toLowerCase(), contact);
		}
	}
}

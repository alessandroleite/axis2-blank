

# Deploying 3rd Party Dependencies to Internal Maven Repository

```
mvn deploy:deploy-file
-Durl=file:file:maven-repo/releases -Dfile=<a jar> -DgroupId=<a group> -DartifactId=<an artifactid> -Dversion=<a version> -Dpackaging=jar
```

Existing 3rd Party Dependencies
-------------

		<dependency>
			 <groupId>com.godigital.quality</groupId>
			 <artifactId>goquality-core</artifactId>
			 <version>2.6.5</version>
		</dependency>

		<dependency>
			 <groupId>com.godigital.quality</groupId>
			 <artifactId>goquality-control</artifactId>
			 <version>2.6.0</version>
		</dependency>